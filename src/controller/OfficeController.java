package controller;

import java.io.IOException;
import java.util.List;
import service.OfficeService;
import entity.Office;

public class OfficeController {

  private OfficeService officeService;

  public void setOfficeService(OfficeService officeService) {
    this.officeService = officeService;
  }

  public List<Office> getOffices() throws IOException {
    return officeService.getOffices();
  }
}
