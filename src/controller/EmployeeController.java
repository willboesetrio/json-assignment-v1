package controller;

import java.io.IOException;
import java.util.List;
import service.EmployeeService;
import entity.Employee;


public class EmployeeController {

  private EmployeeService employeeService;

  public void setEmployeeService(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  public List<Employee> getEmployees() throws IOException {
    return employeeService.getEmployees();
  }
}
