package service;

import dao.EmployeeDao;
import java.io.IOException;
import java.util.List;
import entity.Employee;

public class EmployeeServiceImpl implements EmployeeService {

  private EmployeeDao employeeDao;

  public void setEmployeeDao(EmployeeDao employeeDao) {
    this.employeeDao = employeeDao;
  }

  public List<Employee> getEmployees() throws IOException {
    return employeeDao.getEmployees();
  }
}
