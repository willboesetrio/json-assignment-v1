package service;

import java.io.IOException;
import java.util.List;
import entity.Office;

public interface OfficeService {
  List<Office> getOffices() throws IOException;
}