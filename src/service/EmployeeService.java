package service;

import java.io.IOException;
import java.util.List;
import entity.Employee;

public interface EmployeeService {
  List<Employee> getEmployees() throws IOException;
}

