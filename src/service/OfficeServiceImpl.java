package service;




import dao.OfficeDao;
import entity.Office;

import java.io.IOException;
import java.util.List;

public class OfficeServiceImpl implements OfficeService {
  private OfficeDao officeDao;

  public void setOfficeDao(OfficeDao officeDao) {
    this.officeDao = officeDao;
  }

  public List<Office> getOffices() throws IOException { return officeDao.getOffices(); }

}
