package dao;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;
import entity.Office;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OfficeData implements OfficeDao {

  public List<Office> getOffices() throws IOException {
    String office1 = "{\"Name\":\"Office One\",\"City\":\"Baltimore\",\"State\":\"MD\",\"EstablishedDate\":\"2019-09-01T06:00:00.000Z\"}";
    String office2 = "{\"Name\":\"Office Two\",\"City\":\"Englewood\",\"State\":\"CO\",\"EstablishedDate\":\"2019-09-01T06:00:00.000Z\"}";

    Moshi moshi = new Moshi.Builder()
        .add(Date.class, new Rfc3339DateJsonAdapter())
        .build();
    JsonAdapter<Office> jsonAdapter = moshi.adapter(Office.class);

    List<Office> officeList = new ArrayList<>();

    officeList.add(jsonAdapter.fromJson(office1));
    officeList.add(jsonAdapter.fromJson(office2));

    return officeList;
  }
}