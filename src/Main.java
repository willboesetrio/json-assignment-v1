import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;
import controller.EmployeeController;
import dao.EmployeeCsvData;
import dao.EmployeeXmlData;
import java.io.IOException;
import java.util.Date;
import entity.*;
import java.util.List;
import service.EmployeeServiceImpl;
import controller.OfficeController;
import service.OfficeServiceImpl;
import dao.OfficeData;

public class Main {

  public static void main(String[] args) throws IOException {

      // initialize json adapter
      Moshi moshi = new Moshi.Builder()
          .add(Date.class, new Rfc3339DateJsonAdapter())
          .build();
      JsonAdapter<Employee> jsonEmployee = moshi.adapter(Employee.class);

      OfficeController officeControllerImpl = new OfficeController();
      OfficeServiceImpl officeServiceImpl = new OfficeServiceImpl();
      OfficeData officeData = new OfficeData();

      officeControllerImpl.setOfficeService(officeServiceImpl);
      officeServiceImpl.setOfficeDao(officeData);

      List<Office> officeList = officeControllerImpl.getOffices();

      JsonAdapter<Office> jsonOffice = moshi.adapter(Office.class);

      // loop through offices and write each out to the console
      for (Office office:officeList) {
        System.out.println(jsonOffice.toJson(office));
      }

    }

}